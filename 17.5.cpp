﻿#include <iostream>
#include <cmath>
#include "Vector.h"

Vector::Vector() : x(1), y(2), z(3)
{
}

Vector::Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
{
}

void Vector::Show()
{
	std::cout << "\n" << x << " " << y << " " << z;
}

double Vector::Module()
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}
int main()
{
    Vector v;
    v.Show();
}